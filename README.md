
Annexes accompagnant l'article :
Faire et faire entendre : quand la musique alternative compose avec ses contraintes spatiales. 

Devenue monnaie courante dans la production et la diffusion musicale, l'utilisation de méthodes Do It Yourself est envisagée dans cet article comme une consolidation de coopérations concrètes et pérennes chez les artistes issus de la scène alternative. En analysant l'environnement de création des musiciens puis en décryptant l'état des lieux des salles de spectacles, nous insisterons sur le manque de lieux de représentations de ces artistes en marge et le champ des possibles qu'offrent les espaces situés en dehors des urbanités.

La cartographie des sallesDIY en territoire rural est consultable à cette adresse : 
http://faceboobs.org/lieux_DIY_campagne